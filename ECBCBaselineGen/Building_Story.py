"""helper function to generate Building Story."""
from eppy import modeleditor
import eppy.function_helpers as function_helpers
import itertools


#
# def getZonelistName(baseline_idf,zone_name):
#    ZoneList_objs = baseline_idf.idfobjects["zonelist".upper()]
#    for zonelist in ZoneList_objs:
#        print zonelist.Name

# return zonelist_name

# function to get building Story z axos by zone name
def getStoryNumber(baseline_idf, zone_name):
    # read all surface
    surfaces = baseline_idf.idfobjects['BuildingSurface:Detailed'.upper()]
    # search all surface that belong to specifuc zone name
    zone_surfs = [s for s in surfaces if s.Zone_Name == zone_name]
    # floors = [s for s in zone_surfs if s.Surface_Type.upper() == 'FLOOR']
    # roofs = [s for s in zone_surfs if s.Surface_Type.upper() == 'ROOF']
    # get x y z cordinate for every zone surface
    surf_xyzs = [function_helpers.getcoords(s) for s in zone_surfs]
    surf_xyzs = list(itertools.chain(*surf_xyzs))
    # list all z surfaces
    surf_zs = [z for x, y, z in surf_xyzs]
    # get max and min z surface 
    topz = max(surf_zs)
    botz = min(surf_zs)
    return botz


def get_story_array(z_axis_arr):
    story_arr = []
    i = 1
    while len(z_axis_arr) > 0:
        val = min(z_axis_arr)
        story_arr.append([val, i])
        i = i + 1
        z_axis_arr.pop(z_axis_arr.index(val))
        # print
    return story_arr


def generate_Building_Story(baseline_idf):
    zone_list_array = []
    z_axis_arr = []
    ZoneList_objs = baseline_idf.idfobjects["zonelist".upper()]
    for zonelist in ZoneList_objs:
        #   print zonelist.Name
        for fieldname in zonelist.fieldnames:
            if fieldname.startswith("Zone") and zonelist[fieldname] != "":
                # print "%s = %s" % (fieldname, zonelist[fieldname])
                zone = baseline_idf.getobject('ZONE', zonelist[fieldname])
                zone_area = modeleditor.zonearea(baseline_idf, zone.Name)
                sizing_zone = baseline_idf.getobject('SIZING:ZONE', zonelist[fieldname])

                if (sizing_zone):
                    zone_Design_Specification_Outdoor_Air_Object_Name = sizing_zone.Design_Specification_Outdoor_Air_Object_Name
                    zone_Design_Specification_Zone_Air_Distribution_Object_Name = sizing_zone.Design_Specification_Zone_Air_Distribution_Object_Name
                    zone_type = "conditioned"
                else:
                    zone_Design_Specification_Outdoor_Air_Object_Name = ""
                    zone_Design_Specification_Zone_Air_Distribution_Object_Name = ""
                    zone_type = "unconditioned"
                # z_axis = getStoryNumber(baseline_idf,zone.Name)
                z_axis = zone.Z_Origin
                if z_axis == 0 or z_axis == '':
                    z_axis = getStoryNumber(baseline_idf, zone.Name)
                if zone.Multiplier == '':
                    zone_multiplier = 1
                else:
                    zone_multiplier = zone.Multiplier
                row = [zonelist.Name, zonelist[fieldname], zone_area, zone_type,
                       zone_Design_Specification_Outdoor_Air_Object_Name,
                       zone_Design_Specification_Zone_Air_Distribution_Object_Name, z_axis, zone_multiplier]
                if not (z_axis in z_axis_arr):
                    z_axis_arr.append(z_axis)
                zone_list_array.append(row)
    # print z_axis_arr
    story_arr = get_story_array(z_axis_arr)

    # update story numebr in array
    for row in zone_list_array:
        #  print row
        story_num = [s[1] for s in story_arr if s[0] == row[6]]
        row[6] = story_num[0]

    #    for row in zone_list_array:
    #        print row
    return zone_list_array
