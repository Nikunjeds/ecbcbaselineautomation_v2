from eppy import modeleditor
from eppy.modeleditor import IDF
import sys
import json
from eppy.results import readhtml
from ExtractTableData.Constants import get_constants
from UpdateSimulationControl import removeObjects


def get_Chiller_ReferenceCapacity(Proposed_Idf, ObjectName):
    Chiller_Capacity_A = 0
    Chiller_Capacity_W = 0
    chiller_objs = Proposed_Idf.idfobjects[ObjectName.upper()]
    if len(chiller_objs) > 0:
        for chiller_obj in chiller_objs:
            condenser_Type = chiller_obj.Condenser_Type
            if condenser_Type == "AirCooled":
                Chiller_Capacity_A += float(chiller_obj.Reference_Capacity)

            elif condenser_Type == "WaterCooled" or condenser_Type == "":
                Chiller_Capacity_W += float(chiller_obj.Reference_Capacity)
    # print str(Chiller_Capacity_A) + '\t' + str(Chiller_Capacity_W)
    return Chiller_Capacity_A, Chiller_Capacity_W


def get_Chiller_NominalCapacity(Proposed_Idf, ObjectName):
    Chiller_Capacity_A = 0
    Chiller_Capacity_W = 0
    chiller_objs = Proposed_Idf.idfobjects[ObjectName.upper()]
    if len(chiller_objs) > 0:
        for chiller_obj in chiller_objs:
            condenser_Type = chiller_obj.Condenser_Type
            if condenser_Type == "AirCooled":
                Chiller_Capacity_A += float(chiller_obj.Nominal_Capacity)

            elif condenser_Type == "WaterCooled" or condenser_Type == "":
                Chiller_Capacity_W += float(chiller_obj.Nominal_Capacity)
    # print str(Chiller_Capacity_A) + '\t' + str(Chiller_Capacity_W)
    return Chiller_Capacity_A, Chiller_Capacity_W


def Read_Output_HTML(Output_HTML, ECBC_standard_Idf):
    # print  "Reading Chiller:Electric:EIR table..."
    filehandle = open(Output_HTML, 'r').read()  # get a file handle to the html file
    htables = readhtml.titletable(filehandle)  # reads the tables with
    tbl = [htable for htable in htables if htable[0] in ["Chiller:Electric:EIR"]]
    a_tbl = tbl[0][1]
    ECBC_Total_Cap_Chiller = a_tbl[1][2]
    # print tbl
    # print ECBC_Total_Cap_Chiller
    return ECBC_Total_Cap_Chiller


def Check_Ratio_Air_Water_Cooled_Chillers(Proposed_Idf):
    Chiller_Capacity_A = 0
    Chiller_Capacity_W = 0
    Total_Chiller_Capacity_A = 0
    Total_Chiller_Capacity_W = 0
    Chiller_Capacity_A, Chiller_Capacity_W = get_Chiller_ReferenceCapacity(Proposed_Idf, "Chiller:Electric:EIR")
    Total_Chiller_Capacity_A += Chiller_Capacity_A
    Total_Chiller_Capacity_W += Chiller_Capacity_W
    Chiller_Capacity_A, Chiller_Capacity_W = get_Chiller_ReferenceCapacity(Proposed_Idf,
                                                                           "Chiller:Electric:ReformulatedEIR")
    Total_Chiller_Capacity_A += Chiller_Capacity_A
    Total_Chiller_Capacity_W += Chiller_Capacity_W
    Chiller_Capacity_A, Chiller_Capacity_W = get_Chiller_NominalCapacity(Proposed_Idf, "Chiller:Electric")
    Total_Chiller_Capacity_A += Chiller_Capacity_A
    Total_Chiller_Capacity_W += Chiller_Capacity_W
    Chiller_Capacity_A, Chiller_Capacity_W = get_Chiller_NominalCapacity(Proposed_Idf, "Chiller:Absorption:Indirect")
    Total_Chiller_Capacity_A += Chiller_Capacity_A
    Total_Chiller_Capacity_W += Chiller_Capacity_W
    Chiller_Capacity_A, Chiller_Capacity_W = get_Chiller_NominalCapacity(Proposed_Idf, "Chiller:Absorption")
    Total_Chiller_Capacity_A += Chiller_Capacity_A
    Total_Chiller_Capacity_W += Chiller_Capacity_W
    Chiller_Capacity_A, Chiller_Capacity_W = get_Chiller_NominalCapacity(Proposed_Idf, "Chiller:ConstantCOP")
    Total_Chiller_Capacity_A += Chiller_Capacity_A
    Total_Chiller_Capacity_W += Chiller_Capacity_W
    Chiller_Capacity_A, Chiller_Capacity_W = get_Chiller_NominalCapacity(Proposed_Idf, "Chiller:EngineDriven")
    Total_Chiller_Capacity_A += Chiller_Capacity_A
    Total_Chiller_Capacity_W += Chiller_Capacity_W
    Chiller_Capacity_A, Chiller_Capacity_W = get_Chiller_NominalCapacity(Proposed_Idf, "Chiller:CombustionTurbine")
    Total_Chiller_Capacity_A += Chiller_Capacity_A
    Total_Chiller_Capacity_W += Chiller_Capacity_W
    Chiller_Capacity_A, Chiller_Capacity_W = get_Chiller_NominalCapacity(Proposed_Idf,
                                                                         "ChillerHeater:Absorption:DirectFired")
    Total_Chiller_Capacity_A += Chiller_Capacity_A
    Total_Chiller_Capacity_W += Chiller_Capacity_W
    Chiller_Capacity_A, Chiller_Capacity_W = get_Chiller_NominalCapacity(Proposed_Idf,
                                                                         "ChillerHeater:Absorption:DoubleEffect")
    Total_Chiller_Capacity_A += Chiller_Capacity_A
    Total_Chiller_Capacity_W += Chiller_Capacity_W
    # Chiller_AW_Ratio = Total_Chiller_Capacity_A / Total_Chiller_Capacity_W
    return  Total_Chiller_Capacity_A, Total_Chiller_Capacity_W


def StandardCase_Chiller_Distribution(ECBC_Total_Cap_Chiller, Proposed_Cap_Chiller_A, Proposed_Cap_Chiller_W,
                                      AirCooledMandate, CompilanceLvl):
    # This function will evaluate whether Proposed Chillers meet the Chiller Distribution Requirements. The function will set chiller distribution as required by Standard case.

    # Cap_Chiller_W Water Cooled Chiller Capacity
    # Cap_Chiller_A Air Cooled Chiller Capacity
    # AirCooledMandate Mandatory requirement for Air Cooled Chiller (Y/N)
    # Proposed_Cap_Chiller_W Proposed Case Water Cooled Chiller Capacity
    # Proposed_Cap_Chiller_A Proposed Case Air Cooled Chiller Capacity
    # Proposed_Total_Cap_Chiller
    # CompilanceLvl Compliance Level (ECBC, ECBC+, S_ECBC)

    Proposed_Total_Cap_Chiller = Proposed_Cap_Chiller_W + Proposed_Cap_Chiller_A
    Proposed_Chiller_A_Share = float(Proposed_Cap_Chiller_A) / float(Proposed_Total_Cap_Chiller)
    Proposed_Chiller_W_Share = 1 - Proposed_Chiller_A_Share
    ECBC_Chiller_A_Share = Proposed_Chiller_A_Share
    ECBC_Chiller_W_Share = Proposed_Chiller_W_Share

    if CompilanceLvl == 'ECBC':
        # print ("check 1")
        if ECBC_Total_Cap_Chiller > 530:
            # print ("check 2")
            if AirCooledMandate == 'N' and Proposed_Chiller_A_Share > 0.33:
                # print ("check 3")
                ECBC_Chiller_A_Share = 0.33
                ECBC_Chiller_W_Share = 1 - ECBC_Chiller_A_Share
        # else:
        #  pass
    # else:
    #   pass
    else:
        # print ("check 4")
        ECBC_Chiller_A_Share = 0
        ECBC_Chiller_W_Share = 1 - ECBC_Chiller_A_Share
    return ECBC_Chiller_A_Share, ECBC_Chiller_W_Share


def Set_Chiller_Objects_AirCooled(Cap_Chiller_A):
    if Cap_Chiller_A > 260:
        ChillerObjTyp = "ElectricScrewChiller"
        ChillerObjCount = Cap_Chiller_A / 703
        if ChillerObjCount > 1:

            if Cap_Chiller_A % 703 != 0:
                ChillerObjCount = int(ChillerObjCount) + 1
            else:
                pass
            ChillerObjCap = Cap_Chiller_A / ChillerObjCount
        else:
            ChillerObjCount = 2
            ChillerObjCap = Cap_Chiller_A / ChillerObjCount
    else:
        ChillerObjTyp = "ElectricReciprocatingChiller"
        ChillerObjCount = 1
        ChillerObjCap = Cap_Chiller_A
    ECBC_Reference_COP = Read_ECBC_Efficiency_FromTable('ACC', ChillerObjCap)
    return ChillerObjTyp, ChillerObjCount, ChillerObjCap, ECBC_Reference_COP


def Set_Chiller_Objects_WaterCooled(Cap_Chiller_W):
    if Cap_Chiller_W > 2110:
        ChillerObjTyp = "Centrifugal"
        ChillerObjCount = Cap_Chiller_W / 2813
        if ChillerObjCount > 1:
            ChillerObjCount = int(ChillerObjCount)
            if Cap_Chiller_W % 2813 != 0:
                ChillerObjCount += 1
            else:
                pass
            ChillerObjCap = Cap_Chiller_W / ChillerObjCount
        else:
            ChillerObjCount = 2
            ChillerObjCap = Cap_Chiller_W / ChillerObjCount
    else:
        ChillerObjTyp = "ElectricScrewChiller"
        if Cap_Chiller_W >= 1055:
            ChillerObjCount = 2
            ChillerObjCap = Cap_Chiller_W / 2
        else:
            ChillerObjCount = 1
            ChillerObjCap = Cap_Chiller_W
    ECBC_Reference_COP = Read_ECBC_Efficiency_FromTable('WCC', ChillerObjCap)
    return ChillerObjTyp, ChillerObjCount, ChillerObjCap, ECBC_Reference_COP


def Read_ECBC_Efficiency_FromTable(AWType, ChillerObjCap):
    tbl_rows = get_constants(table='Tbl_5_1_Eff', Type=AWType, Compliance='ECBC')

    ECBC_Reference_COP = 0
    for y in range(0, len(tbl_rows)):
        if tbl_rows[y]["Max_kWr_"] != '':
            if (ChillerObjCap > float(tbl_rows[y]["Min_kWr_"]) and ChillerObjCap <= float(tbl_rows[y]["Max_kWr_"])):
                ECBC_Reference_COP = tbl_rows[y]["COP"]
                break
        else:
            if (ChillerObjCap > float(tbl_rows[y]["Min_kWr_"])):
                ECBC_Reference_COP = tbl_rows[y]["COP"]
                break
    return ECBC_Reference_COP


def add_Water_Plant_Chiller(ECBC_standard_Idf, ECBC_VAV_Library, ECBC_WaterCooled_Chiller_Details):
    ChillerObjCount = int(ECBC_WaterCooled_Chiller_Details[1])
    # Chiller_Name,
    Chiller_Type = ECBC_WaterCooled_Chiller_Details[0]
    Chiller_Capacity = ECBC_WaterCooled_Chiller_Details[2]
    Reference_COP = ECBC_WaterCooled_Chiller_Details[3]
    CondenserType = "WaterCooled"
    for x in range(0, ChillerObjCount):
        if Chiller_Capacity != 0:
            print "Creating HVACTemplate:Plant:Chiller " + "ECBC_WaterCooled_Chiller_" + str(x + 1) + "..."
            new_WaterCooled_ChillerPlant_obj = ECBC_VAV_Library.idfobjects["HVACTemplate:Plant:Chiller".upper()][0]
            new_WaterCooled_ChillerPlant_obj.Name = "ECBC_WaterCooled_Chiller_" + str(x + 1)
            new_WaterCooled_ChillerPlant_obj.Chiller_Type = Chiller_Type
            new_WaterCooled_ChillerPlant_obj.Capacity = Chiller_Capacity * 1000
            new_WaterCooled_ChillerPlant_obj.Nominal_COP = Reference_COP
            new_WaterCooled_ChillerPlant_obj.Condenser_Type = CondenserType
            ECBC_standard_Idf.copyidfobject(new_WaterCooled_ChillerPlant_obj)
            ECBC_standard_Idf.save()
    return ECBC_standard_Idf


def add_Air_Plant_Chiller(ECBC_standard_Idf, ECBC_VAV_Library, ECBC_AirCooled_Chiller_Details):
    ChillerObjCount = int(ECBC_AirCooled_Chiller_Details[1])
    # Chiller_Name,
    Chiller_Type = ECBC_AirCooled_Chiller_Details[0]
    Chiller_Capacity = ECBC_AirCooled_Chiller_Details[2]
    Reference_COP = ECBC_AirCooled_Chiller_Details[3]
    CondenserType = "AirCooled"
    for x in range(0, ChillerObjCount):
        if Chiller_Capacity != 0:
            print "Creating HVACTemplate:Plant:Chiller " + "ECBC_AirCooled_Chiller_" + str(x + 1) + "..."
            new_AirCooled_ChillerPlant_obj = ECBC_VAV_Library.idfobjects["HVACTemplate:Plant:Chiller".upper()][0]
            new_AirCooled_ChillerPlant_obj.Name = "ECBC_AirCooled_Chiller_" + str(ChillerObjCount + 1)
            new_AirCooled_ChillerPlant_obj.Chiller_Type = Chiller_Type
            new_AirCooled_ChillerPlant_obj.Capacity = Chiller_Capacity * 1000
            new_AirCooled_ChillerPlant_obj.Nominal_COP = Reference_COP
            new_AirCooled_ChillerPlant_obj.Condenser_Type = CondenserType
            ECBC_standard_Idf.copyidfobject(new_AirCooled_ChillerPlant_obj)
            ECBC_standard_Idf.save()
    return ECBC_standard_Idf


def post_Run_Processing_System_C(Proposed_Idf, ECBC_standard_Idf, Output_HTML, ECBC_VAV_Library):
    Proposed_Cap_Chiller_A, Proposed_Cap_Chiller_W = Check_Ratio_Air_Water_Cooled_Chillers(
        Proposed_Idf)
    # print str(Chiller_AW_Ratio) +'\t' + str(Proposed_Cap_Chiller_A) +'\t' + str(Proposed_Cap_Chiller_W)
   # Proposed_Cap_Chiller_A = 40
   # Proposed_Cap_Chiller_W = 60
    #if Proposed_Cap_Chiller_A == 0:
     #   Proposed_Cap_Chiller_Ratio
    #Proposed_Cap_ChillerA_Ratio = Proposed_Cap_Chiller_A / (Proposed_Cap_Chiller_W + Proposed_Cap_Chiller_A)
    #Proposed_Cap_ChillerW_Ratio = Proposed_Cap_Chiller_W / (Proposed_Cap_Chiller_W + Proposed_Cap_Chiller_A)
    ECBC_Total_Cap_Chiller = Read_Output_HTML(Output_HTML, ECBC_standard_Idf)
    ECBC_Total_Cap_Chiller_KW = ECBC_Total_Cap_Chiller / 1000
    AirCooledMandate = 'N'  # user input from BEP- EMIS
    CompilanceLvl = 'ECBC'  # user input from BEP- EMIS

    # ECBC_Total_Cap_Chiller_KW = 10000

    ECBC_Chiller_A_Share, ECBC_Chiller_W_Share = StandardCase_Chiller_Distribution(ECBC_Total_Cap_Chiller_KW,
                                                                                   Proposed_Cap_Chiller_A,
                                                                                   Proposed_Cap_Chiller_W,
                                                                                   AirCooledMandate, CompilanceLvl)

    ECBC_Chiller_A_Capacity = ECBC_Total_Cap_Chiller_KW * ECBC_Chiller_A_Share
    ECBC_Chiller_W_Capacity = ECBC_Total_Cap_Chiller_KW - ECBC_Chiller_A_Capacity
    ChillerObjTyp, ChillerObjCount, ChillerObjCap, ECBC_Reference_COP = Set_Chiller_Objects_AirCooled(
        ECBC_Chiller_A_Capacity)
    ECBC_AirCooled_Chiller_Details = [ChillerObjTyp, ChillerObjCount, ChillerObjCap, ECBC_Reference_COP]

    ChillerObjTyp, ChillerObjCount, ChillerObjCap, ECBC_Reference_COP = Set_Chiller_Objects_WaterCooled(
        ECBC_Chiller_W_Capacity)
    ECBC_WaterCooled_Chiller_Details = [ChillerObjTyp, ChillerObjCount, ChillerObjCap, ECBC_Reference_COP]

    ECBC_standard_Idf = removeObjects(ECBC_standard_Idf, "HVACTemplate:Plant:Chiller")
    ECBC_standard_Idf = add_Water_Plant_Chiller(ECBC_standard_Idf, ECBC_VAV_Library, ECBC_WaterCooled_Chiller_Details)
    ECBC_standard_Idf = add_Air_Plant_Chiller(ECBC_standard_Idf, ECBC_VAV_Library, ECBC_AirCooled_Chiller_Details)
    ECBC_standard_Idf.save()
    return  ECBC_standard_Idf

#    print str(ECBC_Chiller_A_Capacity) + '\t' + str (ECBC_Chiller_W_Capacity)
#    print ECBC_AirCooled_Chiller_Details
#    print ECBC_WaterCooled_Chiller_Details


# ECBC_Chiller_A_Capacity, ECBC_Chiller_W_Capacity return to System C expIdf Function
