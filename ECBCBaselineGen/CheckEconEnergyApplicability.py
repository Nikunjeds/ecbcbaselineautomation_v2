#BuiltUpArea, Climate in short form, Occupancy-- user input
def Check_Econ_EnergyRecov_Applicabilty(ECBC_standard_Idf, BuiltUpArea, Climate, Occupancy, Typology):
    print "Check Econ & EnergyRecov Applicabilty..."
    HVAC_VAV_Systems = ECBC_standard_Idf.idfobjects["HVACTemplate:System:VAV".upper()]
    EconApplicability = 'y'
    if BuiltUpArea <= 20000 or Climate == "WH" or (Climate=="HD" and Occupancy == "Daytime"):   #inputs from user
        EconApplicability = 'n'
    
    EnerRecovApplicability = 'y'
    if Typology!="Hospitality" or Typology!="Healthcare":   #inputs from user
        EnerRecovApplicability = 'n'
    
    for VAV_System in HVAC_VAV_Systems:
        #Econ_Applicability.
        if EconApplicability == 'y':
            if VAV_System.Supply_Fan_Maximum_Flow_Rate > 3.2 :
                VAV_System.Economizer_Type="FixedDryBulb"
                VAV_System.Economizer_lockout="NoLockout"
                VAV_System.Economizer_UpperTempaerature_Limit=24
                VAV_System.Economizer_LowerTempaerature_Limit=4
                ECBC_standard_Idf.save()
        if EnerRecovApplicability == 'y': 
            maximum_Outdoor_Air_Flow_Rate = VAV_System.Maximum_Outdoor_Air_Flow_Rate
            supply_Fan_Maximum_Flow_Rate = VAV_System.Supply_Fan_Maximum_Flow_Rate
            outdoorAirRatio = maximum_Outdoor_Air_Flow_Rate/supply_Fan_Maximum_Flow_Rate
            if outdoorAirRatio > 0.7 and supply_Fan_Maximum_Flow_Rate> 2.1:
                VAV_System.Heat_Recovery_type="Sensible"
                VAV_System.Sensible_Heat_Recovery_Effectiveness=0.5
                VAV_System.Latent_Heat_Recovery_Effectiveness=0.5
                ECBC_standard_Idf.save()
    return  ECBC_standard_Idf           


            