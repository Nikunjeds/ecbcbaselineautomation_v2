#Schedule set file

from UpdateBuildingEnvelope import copy_objects
from Create_HVAC_Zone_Array import removeObject


def remove_HVACTemplate_Schedule(ECBC_standard_Idf):
    SchTypeLimits_objs = ECBC_standard_Idf.idfobjects["ScheduleTypeLimits".upper()]
    SchTypeLimits_obs = [obj for obj in SchTypeLimits_objs if obj.Name.upper().find("HVACTEMPLATE") != -1 ] 
    for obj in SchTypeLimits_obs:
        obj.Name = obj.Name + "_old"
        ECBC_standard_Idf.save()
    SchCompact_objs = ECBC_standard_Idf.idfobjects["Schedule:Compact".upper()]
    SchCompact_obs = [obj for obj in SchCompact_objs if obj.Name.upper().find("HVACTEMPLATE") != -1 ] 
    for obj in SchCompact_obs:
        obj.Name = obj.Name + "_old"
        ECBC_standard_Idf.save()
    return ECBC_standard_Idf

def copy_Schedule(ECBC_Schedule_Library,ECBC_standard_Idf):
   # ECBC_standard_Idf= removeObject(ECBC_standard_Idf,"ScheduleTypeLimits".upper())
   # ECBC_standard_Idf= removeObject(ECBC_standard_Idf,"Schedule:Day:Interval".upper())
   # ECBC_standard_Idf= removeObject(ECBC_standard_Idf,"Schedule:Week:Daily".upper())
   # ECBC_standard_Idf= removeObject(ECBC_standard_Idf,"Schedule:Year".upper())
   # ECBC_standard_Idf= removeObject(ECBC_standard_Idf,"Schedule:Constant".upper())
    
    #copy ScheduleTypeLimits to ECBC_standard_Idf
    print "copy ScheduleTypeLimits in ECBC_standard_Idf"
    ECBC_standard_Idf = copy_objects(ECBC_Schedule_Library,ECBC_standard_Idf,"ScheduleTypeLimits".upper())
    #copy Schedule:Day:Interval to ECBC_standard_Idf
    print "copy Schedule:Day:Interval in ECBC_standard_Idf"
    ECBC_standard_Idf = copy_objects(ECBC_Schedule_Library,ECBC_standard_Idf,"Schedule:Day:Interval".upper())
    #copy Schedule:Week:Daily to ECBC_standard_Idf
    print "copy Schedule:Week:Daily in ECBC_standard_Idf"
    ECBC_standard_Idf = copy_objects(ECBC_Schedule_Library,ECBC_standard_Idf,"Schedule:Week:Daily".upper())
    #copy Schedule:Year to ECBC_standard_Idf
    print "copy Schedule:Year in ECBC_standard_Idf"
    ECBC_standard_Idf = copy_objects(ECBC_Schedule_Library,ECBC_standard_Idf,"Schedule:Year".upper())
    #copy Schedule:Constant to ECBC_standard_Idf
    print "copy Schedule:Constant in ECBC_standard_Idf"
    ECBC_standard_Idf = copy_objects(ECBC_Schedule_Library,ECBC_standard_Idf,"Schedule:Constant".upper())
    
    return ECBC_standard_Idf

def update_lights(ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details):
    #read all lights object
    lights_objs = ECBC_standard_Idf.idfobjects["lights".upper()]
    #for every lights object
    for lights in lights_objs:
        lights_name = lights.Name
        #read zonelist name of lights
        zonelist_name = lights.Zone_or_ZoneList_Name
        #search zonelist name in ECBC_Zonelist_Wise_Schedule and read lighting schedule from searched row
        row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == zonelist_name]
        if not row:
            zonelist_name = [zone_row[0] for zone_row in zone_wise_buildingstory_details if
                             zone_row[1] == zonelist_name]
            row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == zonelist_name[0]]

        ECBC_Lighting_Schedule = row[0][2]
        #update schedule name 
        lights.Schedule_Name = ECBC_Lighting_Schedule
        print "Replace schedule of " + lights_name 
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf   

def update_people(ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details):
    #read all people object
    people_objs = ECBC_standard_Idf.idfobjects["people".upper()]
    #for every people object
    for people in people_objs:
        #read zonelist name 
        zonelist_name = people.Zone_or_ZoneList_Name
        #search ECBC People and ECBC activity Schedule from ECBC_Zonelist_Wise_Schedule array
        row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == zonelist_name]
        if not row:
            zonelist_name = [zone_row[0] for zone_row in zone_wise_buildingstory_details if
                             zone_row[1] == zonelist_name]
            row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == zonelist_name[0]]

        ECBC_People_Schedule = row[0][1]
        ECBC_Activity_Schedule = row[0][1]
        #update Schedule
        people.Number_of_People_Schedule_Name = ECBC_People_Schedule
        people.Activity_Level_Schedule_Name=ECBC_Activity_Schedule
        print "Replace schedule of " + people.Name 
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf
        
                
def update_equipment(ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details):
    #read all eqipment object
    ElectricEquipment_objs = ECBC_standard_Idf.idfobjects["ElectricEquipment".upper()]
    #for every Equipment object
    for ElectricEquipment in ElectricEquipment_objs:
        #read zonelist name from equipment object
        zonelist_name = ElectricEquipment.Zone_or_ZoneList_Name
        #search ECBC Equipment Schedule from ECBC_Zonelist_Wise_Schedule array
        row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == zonelist_name]
        if not row:
            zonelist_name = [zone_row[0] for zone_row in zone_wise_buildingstory_details if
                             zone_row[1] == zonelist_name]
            row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == zonelist_name[0]]

        ECBC_Equipment_Schedule = row[0][3]
        #update schedule
        ElectricEquipment.Schedule_Name = ECBC_Equipment_Schedule
        print "Replace schedule of " + ElectricEquipment.Name 
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf
          
            
def update_Infiltration(ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details):
    #read all Infiltration object
    ZoneInfiltration_objs = ECBC_standard_Idf.idfobjects["ZoneInfiltration:DesignFlowRate".upper()]
    #for every infiltration object
    for ZoneInfiltration in ZoneInfiltration_objs:
        #read zonelist name 
        zonelist_name = ZoneInfiltration.Zone_or_ZoneList_Name
         #search ECBC Infiltration Schedule from ECBC_Zonelist_Wise_Schedule array
        row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == zonelist_name]
        if not row:
            zonelist_name = [zone_row[0] for zone_row in zone_wise_buildingstory_details if zone_row[1] == zonelist_name]
            row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == zonelist_name[0]]

        ECBC_Infiltration_Schedule = row[0][5]
        #update schedule name
        ZoneInfiltration.Schedule_Name = ECBC_Infiltration_Schedule
        print "Replace schedule of " + ZoneInfiltration.Name 
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf
    
def update_schedule(ECBC_Schedule_Library,ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details):
    
    print "Copy Standard Schedule to ECBC_standard_Idf"
    copy_Schedule(ECBC_Schedule_Library,ECBC_standard_Idf)
    print "Updating Lights object.."
    ECBC_standard_Idf = update_lights(ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details)
    print "Updating People object.."
    ECBC_standard_Idf = update_people(ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details)
    print "Updating Equipment object.."
    ECBC_standard_Idf = update_equipment(ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details)
    print "Updating ZoneInfiltration:DesignFlowRate object.."
    ECBC_standard_Idf = update_Infiltration(ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details)
    
    return ECBC_standard_Idf
    
    