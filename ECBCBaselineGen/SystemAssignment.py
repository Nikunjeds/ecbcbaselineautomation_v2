"""helper function to Update Baseline System Type based on inputs."""
# from Cr_Baseline import set_IDF
from eppy import modeleditor
from eppy.modeleditor import IDF
from HVAC_SystemType_B import creating_HVAC_SystemType_B
from HVAC_SystemType_C import creating_HVAC_SystemType_C
from HVAC_SystemType_A import creating_HVAC_SystemType_A
from HVAC_SystemType_D import creating_HVAC_SystemType_D


def set_IDF(File_Path):
    iddfile = "C:/EnergyPlusV8-6-0/Energy+.idd"
    IDF.setiddname(iddfile)
    IDF_File = IDF(File_Path)
    return IDF_File


def System_type_assignment(ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details, HeatFuelSrc,
                           cooling_SetPoint):
    appliedSystem = ""
    if HeatFuelSrc == "Elect" or HeatFuelSrc == "No Heating":
        HeatFuelSrc = "Elect"
    else:
        HeatFuelSrc = "Fossil"

    Zonelist_objs = ECBC_standard_Idf.idfobjects['Zonelist'.upper()]
    # Zonelist_objs = [row[0]  for row in zone_wise_buildingstory_details]
    # print Zonelist_objs
    SystemA_Zonelist = []
    SystemD_Zonelist = []
    Rest_Zonelist = []
    TotalConditionedArea = 0
    for Zonelist in Zonelist_objs:
        System_Type = [row[12] for row in ECBC_Zonelist_Wise_Schedule if row[0] == Zonelist.Name]
        # print System_Type
        if System_Type[0] == "Sys A":
            SystemA_Zonelist.append(Zonelist.Name)
            # print "System A"
        elif System_Type[0] == "Sys D":
            SystemD_Zonelist.append(Zonelist.Name)
            # print "System D"
        else:
            Rest_Zonelist.append(Zonelist.Name)
            Zone_objs = [row for row in zone_wise_buildingstory_details if
                         Zonelist.Name == row[0] and row[3] == "conditioned"]
            # print "System B & D"
            for Zone in Zone_objs:
                zone_area = Zone[2]
                zone_multiplier = Zone[7]
                # print Zonelist.Name + " " + str(Zone)
                TotalConditionedArea = TotalConditionedArea + (zone_area * zone_multiplier)

    if len(SystemA_Zonelist) > 0:
        print "Setting SystemType A in zones.."
        print SystemA_Zonelist
        ECBC_SystemType_Library_Name = "SysA_Heat" + HeatFuelSrc
        ECBC_SystemType_Library = set_IDF("./LibraryFiles/HVAC/" + ECBC_SystemType_Library_Name + ".idf")
        ECBC_standard_Idf = creating_HVAC_SystemType_A(ECBC_standard_Idf, SystemA_Zonelist, ECBC_SystemType_Library,
                                                       ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details,
                                                       cooling_SetPoint)
        # ApplySysType(SystemType_FunctionName, SystemA_Zonelist)
        appliedSystem += "A"

    if len(SystemD_Zonelist) > 0:
        print "Setting SystemType D in zones.."
        print SystemD_Zonelist
        ECBC_SystemType_Library_Name = "SysD"
        ECBC_SystemType_Library = set_IDF("./LibraryFiles/HVAC/" + ECBC_SystemType_Library_Name + ".idf")
        ECBC_standard_Idf = creating_HVAC_SystemType_D(ECBC_standard_Idf, SystemD_Zonelist, ECBC_SystemType_Library,
                                                       ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details,
                                                       cooling_SetPoint)
        # ApplySysType("SysD", SystemD_Zonelist)
        appliedSystem += "D"

    if len(Rest_Zonelist) > 0:
        print "Total Conditioned area for the rest zone is : " + str(TotalConditionedArea)
        if TotalConditionedArea < 12500:  # should be <
            print "Setting SystemType B in zones.."
            print Rest_Zonelist
            ECBC_SystemType_Library_Name = "SysB_Heat" + HeatFuelSrc + "_HeatRecovN"
            ECBC_SystemType_Library = set_IDF("./LibraryFiles/HVAC/" + ECBC_SystemType_Library_Name + ".idf")
            ECBC_standard_Idf = creating_HVAC_SystemType_B(ECBC_standard_Idf, Rest_Zonelist, ECBC_SystemType_Library,
                                                           ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details,
                                                           cooling_SetPoint)
            ##ApplySysType(SystemType_FunctionName, Rest_Zonelist)
            appliedSystem += "B"

        else:
            print "Setting SystemType C in zones.."
            print Rest_Zonelist
            ECBC_SystemType_Library_Name = "SysC_Heat" + HeatFuelSrc + "_EconN_HeatRecovN"
            ECBC_SystemType_Library = set_IDF("./LibraryFiles/HVAC/" + ECBC_SystemType_Library_Name + ".idf")
            ECBC_standard_Idf = creating_HVAC_SystemType_C(ECBC_standard_Idf, Rest_Zonelist, ECBC_SystemType_Library,
                                                           ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details,
                                                           cooling_SetPoint)
            ##ApplySysType(SystemType_FunctionName, Rest_Zonelist)
            appliedSystem += "C"

    return ECBC_standard_Idf, appliedSystem, ECBC_SystemType_Library
