"""helper function to get ECBC Schedule and zonelist from csv"""
#function to read csv file nd generate two list - Standard ECBC zonelist name and zonelist wise Schedule name
def get_ECBC_Zonelist():
    ECBC_Zonelist_csv = "./csv/ECBC_Zonelist.csv"
    ECBC_Zonelist = []
    ECBC_Zonelist_Wise_Schedule = []
    #open csv file for reading
    file = open(ECBC_Zonelist_csv,"r")
    i = 1
    #for every line other than header row
    for line in file: 
        if i == 1 :
            #skip header row
            i= i+ 1
        else:    
            #Read line and split line by ',' 
            data = line.split(',')
            #append zonelist name to array
            ECBC_Zonelist.append(data[0].strip())
            arr = []
            #add all data to temp array
            for val in data:
                arr.append(val.strip())  #= [data[0].strip(),data[1].strip(),data[2].strip(),data[3].strip(),data[3].strip()]  
            #append row to array
            ECBC_Zonelist_Wise_Schedule.append(arr)
    #print ECBC_Zonelist  
    #print ECBC_ZoneListWiseSchedule
    return ECBC_Zonelist,ECBC_Zonelist_Wise_Schedule

#function to read remove objec list csv and store in an array
def get_ECBC_Object_Remove_list():
    ECBC_Object_Remove_csv = "./csv/ECBC_Object_Remove.csv"
    ECBC_Object_Remove_list = []
    file = open(ECBC_Object_Remove_csv,"r")
    for line in file:         
        data = line.split(',')
        ECBC_Object_Remove_list.append(data[0].strip())
       
    #print ECBC_Zonelist  
    #print ECBC_ZoneListWiseSchedule
    return ECBC_Object_Remove_list

       