"""helper function for checking Lights objects and replacing Design level Calculation Method and Zonelist Name"""

#function to check & update Lighting 'Design_Level_Calculation_Method' and 'LPD'
def update_lighting(ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule):   
    #read all lights object
    lights_objs = ECBC_standard_Idf.idfobjects["lights".upper()]
    #for each light check Design_Level_Calculation_Method and set LPD values
    for lights in lights_objs:
        if lights.Design_Level_Calculation_Method != "Watts/Area":
            lights.Design_Level_Calculation_Method = "Watts/Area"
        zonelist_name = lights.Zone_or_ZoneList_Name   
        # get lpd value based on Zonelist Name from function 
        
        lpd_value = getLPDValue(zonelist_name,ECBC_Zonelist_Wise_Schedule)
        #print lpd_value       
        #set lpd value
        lights.Watts_per_Zone_Floor_Area = lpd_value     
        #print lights.Watts_per_Zone_Floor_Area
    print "Light check Complete"
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf
        
# function to get LPD values from ECBC_Zonelist_Wise_Schedule by passing zonelist name    
def getLPDValue(zonelist_name, ECBC_Zonelist_Wise_Schedule):
    row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == zonelist_name]   
    print zonelist_name
    lpd_value = row[0][11]
    return lpd_value
        
    
    
    