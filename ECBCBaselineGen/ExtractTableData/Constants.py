import csv
from os import listdir
from os.path import isfile, join, splitext
import json
from collections import OrderedDict
import re
import copy

constants_dir = "./ExtractTableData/"
filenames = [f for f in listdir(constants_dir) if isfile(join(constants_dir, f))]
json_constants = {}

for filename in filenames:
    filename_clean,extension = splitext(filename)
    if extension != ".csv":
        continue
 #   print "Got Table : "+filename_clean
    with open(join(constants_dir,filename), "rb") as f:
        json_constants[filename_clean]=[]
        reader = csv.reader(f, delimiter='\t')
        json_template = {}
        keys = next(reader)
        keys_cleaned = []
        for key in keys:
            keys_cleaned.append(re.sub(r'\W+', '_', key))
        rows = [OrderedDict(zip(keys_cleaned,row)) for row in reader ]
        for line in rows:                
            json_constants[filename_clean].append(line)


def get_constants(**kwargs):
    flag_union=True
    if len(kwargs) < 1:
        print "Needs at least table name"
        return
    table_name = kwargs.pop('table')
    table = json_constants[table_name]
    if len(kwargs)>0:
        final_table=copy.deepcopy(table)
        for key in kwargs:
            for i, row in enumerate(table):
                if key not in row and row in final_table:
                    final_table.remove(row)
                elif row[key]!= kwargs[key] and row in final_table:
                    final_table.remove(row)
        return final_table
    return table

#print json.dumps(get_constants(table='Tbl_4_SHGCN15',Compliance='ECBC')[0]["Com"])
#print json.dumps(get_constants(table='Tbl_5_1_Eff'))
