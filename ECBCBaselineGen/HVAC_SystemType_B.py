from UpdateBuildingEnvelope import copy_objects
from Create_HVAC_Zone_Array import remove_Object_From_CSV

"""helper function to create HVAC System type B"""

#function to craete a HVAC sizing zone list with coresponding values (Design_Specification_Outdoor_Air_Object_Name, Design_Specification_Zone_Air_Distribution_Object_Name, fan schedule, heating setpoint and cooling setpoint) 
def get_HVAC_Zone_List(ECBC_standard_Idf,System_Zonelist, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details):
    #array to store zone wise information and to store unique schedule name
    HVAC_Zone_List = []
    HVAC_unique_schedule = []
    #Read all sizing zone
    #print zone_wise_buildingstory_details
    for Zonelist in  System_Zonelist:
        # print Zonelist
        #find zonelist name from zone_wise_buildingstory_details array 
        zones_list = [row for row in zone_wise_buildingstory_details if row[0] == Zonelist and  row[3] == "conditioned"]
        # print zones_list
        # find row containg zone detail from ECBC_Zonelist_Wise_Schedule array    
        row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == Zonelist]
        # print row
        #read all required data
        ECBC_fanSchedule =row[0][7]
        ECBC_heatingSetpoint =row[0][8]
        ECBC_coolingSetpoint =row[0][9]
        # store schedule name in unique schedule array if not exist
        if not(ECBC_fanSchedule in HVAC_unique_schedule):
            HVAC_unique_schedule.append(ECBC_fanSchedule)
            
        for zones_details in zones_list:    
            #print zones_details
            zonelist_name = zones_details[0]
            
            zone_name = zones_details[1]
            #print zone_name
            zone_Design_Specification_Outdoor_Air_Object_Name = zones_details[4]
            zone_Design_Specification_Zone_Air_Distribution_Object_Name = zones_details[5]
            HVAC_Zone_Details = [zone_name, zone_Design_Specification_Outdoor_Air_Object_Name, zone_Design_Specification_Zone_Air_Distribution_Object_Name, ECBC_fanSchedule, ECBC_heatingSetpoint, ECBC_coolingSetpoint]
            HVAC_Zone_List.append(HVAC_Zone_Details)
            
   
    #print HVAC_Zone_List
    return HVAC_Zone_List,  HVAC_unique_schedule   

#function to add sizing parameter object
def add_Sizing_Parameter(ECBC_standard_Idf, ECBC_VRF_Library, heating_Sizing_Factor, cooling_Sizing_Factor):
    print "Creating Sizing:Parameter object...."
    #copy sizing parameter object from standard idf to ECBC_standard_Idf and set its attribute value and save
    sizing_parameter_objs = ECBC_VRF_Library.idfobjects["Sizing:Parameters".upper()][0]
    sizing_parameter_objs.Heating_Sizing_Factor=heating_Sizing_Factor
    sizing_parameter_objs.Cooling_Sizing_Factor=cooling_Sizing_Factor
    sizing_parameter_objs.Timesteps_in_Averaging_Window=""
    ECBC_standard_Idf.copyidfobject(sizing_parameter_objs)
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf

#function to add one HVAC Thermostat object 
def add_HVAC_Thermostat(ECBC_standard_Idf, ECBC_VRF_Library, zone_name, ECBC_heatingSetpoint, ECBC_coolingSetpoint):
    print "Creating HVACTemplate:Thermostat object...."
    #read HVAC thermostat object from standard idf
    new_thermostat_obj = ECBC_VRF_Library.idfobjects["HVACTemplate:Thermostat".upper()][0]
   # ECBC_standard_Idf = copy_objects(ECBC_VRF_Library, ECBC_standard_Idf, "HVACTemplate:Thermostat".upper())
   # hvac_thermostat_objs = ECBC_standard_Idf.idfobjects["HVACTemplate:Thermostat".upper()][0]
    #set schedule from csv
    #set attribute value of objects
    new_thermostat_obj.Name=zone_name
    new_thermostat_obj.Heating_Setpoint_Schedule_Name = ECBC_heatingSetpoint
    new_thermostat_obj.Cooling_Setpoint_Schedule_Name = ECBC_coolingSetpoint
    new_thermostat_obj.Constant_Heating_Setpoint = ""
    new_thermostat_obj.Constant_Cooling_Setpoint = ""
    #copy that object to ECBC_standard_Idf
    ECBC_standard_Idf.copyidfobject(new_thermostat_obj)
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf

#function to add one HVAC zone VRF object 
def add_HVAC_Zone_VRF(ECBC_standard_Idf, ECBC_VRF_Library, zone_name, VRF_Name, design_Specification_Outdoor_Air_Object_Name, design_Specification_Zone_Air_Distribution_Object_Name, ECBC_fanSchedule, cooling_SetPoint):
    
    print "Creating " + zone_name  + " HVACTemplate:Zone:VRF..."

    #standard_objects = S_IDF.idfobjects[objectName]
    #read HVAC zone VRF object from standard idf
    new_VRF_obj = ECBC_VRF_Library.idfobjects["HVACTemplate:Zone:VRF".upper()][0]   #copy_objects(ECBC_VRF_Library, ECBC_standard_Idf, "HVACTemplate:Zone:VRF".upper())
    #hvac_zone_vrf_obj = ECBC_standard_Idf.idfobjects["HVACTemplate:Zone:VRF".upper()][count -1]
    #set attribute value of object and copy in ECBC_standard_Idf
    new_VRF_obj.Zone_Name = zone_name       
    new_VRF_obj.Template_VRF_System_Name = VRF_Name 
    new_VRF_obj.Template_Thermostat_Name = zone_name
    new_VRF_obj.Design_Specification_Outdoor_Air_Object_Name = design_Specification_Outdoor_Air_Object_Name
    new_VRF_obj.Design_Specification_Zone_Air_Distribution_Object_Name =  design_Specification_Zone_Air_Distribution_Object_Name
    new_VRF_obj.Supply_Fan_Operating_Mode_Schedule_Name = ECBC_fanSchedule
    new_VRF_obj.Zone_Cooling_Design_Supply_Air_Temperature = cooling_SetPoint - 11
    
    ECBC_standard_Idf.copyidfobject(new_VRF_obj)

    ECBC_standard_Idf.save()
    # replace fan / Hvac schedule

 
        
    return ECBC_standard_Idf
 
#function to add HVAC System VRF from list passed as parameter    
def add_HVAC_System_VRF(ECBC_standard_Idf,ECBC_VRF_Library, VRF_name_list):
    print "Creating  HVACTemplate:System:VRF object..."
    #for every row in array create object , set its attribute and add to ECBC_standard_Idf
    for VRF_name in VRF_name_list:
        print "Creating " + VRF_name[0]  + " HVACTemplate:System:VRF..."
        new_VRF_System_obj = ECBC_VRF_Library.idfobjects["HVACTemplate:System:VRF".upper()][0]
        new_VRF_System_obj.Name = VRF_name[0]
        new_VRF_System_obj.System_Availability_Schedule_Name = VRF_name[1]
        new_VRF_System_obj.Gross_Rated_Cooling_COP=3.3
        new_VRF_System_obj.Gross_Rated_Heating_COP=3.3
        ECBC_standard_Idf.copyidfobject(new_VRF_System_obj)       
        ECBC_standard_Idf.save()
    return ECBC_standard_Idf    
        
#main function (cooling_SetPoint is user input)
def creating_HVAC_SystemType_B(ECBC_standard_Idf, System_Zonelist, ECBC_VRF_Library, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details, cooling_SetPoint):
    print "get HVAC Zone List"
    #function call to fet zone wise details and unique schedule name
    HVAC_Zone_List, HVAC_unique_schedule = get_HVAC_Zone_List(ECBC_standard_Idf,System_Zonelist, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details)
    
    VRF_name_list = []
   # print HVAC_Zone_List
    VRF_Count = 0
    #ECBC_standard_Idf = remove_Object_From_CSV(ECBC_standard_Idf)
    #ECBC_standard_Idf = add_Sizing_Parameter(ECBC_standard_Idf, ECBC_VRF_Library,  heating_Sizing_Factor, cooling_Sizing_Factor)
    # for every schedule read all zone name and create thermostat, zone VRF object and for each 20 zone in same schedule create one vrf system 
    for HVAC_schedule in HVAC_unique_schedule:        
        zone_list = [row[0]  for row in HVAC_Zone_List if row[3] == HVAC_schedule] 
        #print zone_list
       # print HVAC_schedule
        count = 1
        for zone in zone_list:  
            
            if count == 1:
                VRF_Count = VRF_Count + 1
                VRF_Name = "VRF_System_" + str(VRF_Count)
                VRF_name_list.append([VRF_Name,HVAC_schedule])
            zone_details = [zone_set_row for zone_set_row in HVAC_Zone_List if zone_set_row[0] == zone]
            #zone_name , heeting setpoint, cooling setpoint
            ECBC_standard_Idf = add_HVAC_Thermostat(ECBC_standard_Idf, ECBC_VRF_Library, zone_details[0][0],zone_details[0][4],zone_details[0][5])
           # print "Creating HVACTemplate:Zone:VRF object...."
            ECBC_standard_Idf = add_HVAC_Zone_VRF(ECBC_standard_Idf, ECBC_VRF_Library, zone_details[0][0], VRF_Name, zone_details[0][1], zone_details[0][2], zone_details[0][3], cooling_SetPoint)
            
            count = count + 1
            if count == 21:
                count = 1
    
    print VRF_name_list
    ECBC_standard_Idf = add_HVAC_System_VRF(ECBC_standard_Idf,ECBC_VRF_Library, VRF_name_list)
  
    return ECBC_standard_Idf
        