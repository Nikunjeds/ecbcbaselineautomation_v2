from UpdateBuildingEnvelope import copy_objects
from Create_HVAC_Zone_Array import remove_Object_From_CSV


# function to craete a HVAC sizing zone list with coresponding values (Design_Specification_Outdoor_Air_Object_Name, Design_Specification_Zone_Air_Distribution_Object_Name, fan schedule, heating setpoint and cooling setpoint)
def get_HVAC_Zone_List(ECBC_standard_Idf, System_Zonelist, ECBC_Zonelist_Wise_Schedule,
                       zone_wise_buildingstory_details):
    # array to store zone wise information and to store unique schedule name
    HVAC_Zone_List = []
    building_story_arr = []
    buildingStory_Schedule_arr = []
    # Read all zonelist

    for Zonelist in System_Zonelist:
        # print Zonelist
        # find zonelist name from zone_wise_buildingstory_details array
        zones_list = [row for row in zone_wise_buildingstory_details if row[0] == Zonelist and row[3] == "conditioned"]

        # find row containg zone detail from ECBC_Zonelist_Wise_Schedule array    
        row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == Zonelist]
        # read all required data
        ECBC_fanSchedule = row[0][7]
        ECBC_heatingSetpoint = row[0][8]
        ECBC_coolingSetpoint = row[0][9]
        for zones_details in zones_list:
            # print zones_details
            zonelist_name = zones_details[0]

            zone_name = zones_details[1]
            # print zone_name
            zone_Design_Specification_Outdoor_Air_Object_Name = zones_details[4]
            zone_Design_Specification_Zone_Air_Distribution_Object_Name = zones_details[5]
            building_story = zones_details[6]
            building_schedule = [zones_details[6], ECBC_fanSchedule]
            # store story number in building_story_arr array if not exist
            if not (building_story in building_story_arr):
                building_story_arr.append(building_story)
            if not (building_schedule in buildingStory_Schedule_arr):
                buildingStory_Schedule_arr.append(building_schedule)
            # store value in temp array and append this arry to original array
            HVAC_Zone_Details = [zone_name, zone_Design_Specification_Outdoor_Air_Object_Name,
                                 zone_Design_Specification_Zone_Air_Distribution_Object_Name, ECBC_fanSchedule,
                                 ECBC_heatingSetpoint, ECBC_coolingSetpoint, building_story]
            HVAC_Zone_List.append(HVAC_Zone_Details)
    #        print sizing_zone.Zone_or_ZoneList_Name
    #        print sizing_zone.Design_Specification_Outdoor_Air_Object_Name
    #        print sizing_zone.Design_Specification_Zone_Air_Distribution_Object_Name

    # print buildingStory_Schedule_arr
    return HVAC_Zone_List, building_story_arr, buildingStory_Schedule_arr


# function to add sizing parameter object
def add_Sizing_Parameter(ECBC_standard_Idf, ECBC_VAV_Library, heating_Sizing_Factor, cooling_Sizing_Factor):
    print "Creating Sizing:Parameter object...."
    # copy sizing parameter object from standard idf to ECBC_standard_Idf and set its attribute value and save
    sizing_parameter_objs = ECBC_VAV_Library.idfobjects["Sizing:Parameters".upper()][0]
    sizing_parameter_objs.Heating_Sizing_Factor = heating_Sizing_Factor
    sizing_parameter_objs.Cooling_Sizing_Factor = cooling_Sizing_Factor
    sizing_parameter_objs.Timesteps_in_Averaging_Window = ""
    ECBC_standard_Idf.copyidfobject(sizing_parameter_objs)
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf


# function to add one HVAC Thermostat object
def add_HVAC_Thermostat(ECBC_standard_Idf, ECBC_VAV_Library, zone_name, ECBC_heatingSetpoint, ECBC_coolingSetpoint):
    Thermostat_Name = zone_name + "_Thermostat"
    print "Creating " + Thermostat_Name + " HVACTemplate:Thermostat object...."
    # read HVAC thermostat object from standard idf
    new_thermostat_obj = ECBC_VAV_Library.idfobjects["HVACTemplate:Thermostat".upper()][0]
    # set schedule from csv
    # set attribute value of objects

    new_thermostat_obj.Name = Thermostat_Name
    new_thermostat_obj.Heating_Setpoint_Schedule_Name = ECBC_heatingSetpoint
    new_thermostat_obj.Cooling_Setpoint_Schedule_Name = ECBC_coolingSetpoint
    new_thermostat_obj.Constant_Heating_Setpoint = ""
    new_thermostat_obj.Constant_Cooling_Setpoint = ""
    # copy that object to ECBC_standard_Idf
    ECBC_standard_Idf.copyidfobject(new_thermostat_obj)
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf


# function to add one HVAC zone VRF object
def add_HVAC_Zone_VAV(ECBC_standard_Idf, ECBC_VAV_Library, zone_name, VAV_Name,
                      design_Specification_Outdoor_Air_Object_Name,
                      design_Specification_Zone_Air_Distribution_Object_Name, ECBC_fanSchedule, cooling_SetPoint):
    print "Creating " + zone_name + " HVACTemplate:Zone:VAV..."
    Thermostat_Name = zone_name + "_Thermostat"
    # standard_objects = S_IDF.idfobjects[objectName]
    # read HVAC zone VAV object from standard idf
    new_VAV_obj = ECBC_VAV_Library.idfobjects["HVACTemplate:Zone:VAV".upper()][0]
    new_VAV_obj.Zone_Name = zone_name
    new_VAV_obj.Template_VAV_System_Name = VAV_Name
    new_VAV_obj.Template_Thermostat_Name = Thermostat_Name
    new_VAV_obj.Design_Specification_Outdoor_Air_Object_Name_for_Sizing = design_Specification_Outdoor_Air_Object_Name
    new_VAV_obj.Design_Specification_Zone_Air_Distribution_Object_Name = design_Specification_Zone_Air_Distribution_Object_Name
    # new_VAV_obj.Supply_Fan_Operating_Mode_Schedule_Name = ECBC_fanSchedule
    # new_VAV_obj.Zone_Cooling_Design_Supply_Air_Temperature = float(cooling_SetPoint) - 11

    ECBC_standard_Idf.copyidfobject(new_VAV_obj)
    ECBC_standard_Idf.save()
    # replace fan / Hvac schedule
    return ECBC_standard_Idf


# function to add HVAC System VAV from list passed as parameter
def add_HVAC_System_VAV(ECBC_standard_Idf, ECBC_VAV_Library, VAV_name_list, cooling_SetPoint):
    print "Creating  HVACTemplate:System:VAV object..."
    # for every row in array create object , set its attribute and add to ECBC_standard_Idf
    for VAV_name in VAV_name_list:
        print "Creating " + VAV_name[0] + " HVACTemplate:System:VAV..."
        new_VAV_System_obj = ECBC_VAV_Library.idfobjects["HVACTemplate:System:VAV".upper()][0]
        new_VAV_System_obj.Name = VAV_name[0]
        new_VAV_System_obj.System_Availability_Schedule_Name = VAV_name[1]
        new_VAV_System_obj.Cooling_Coil_Design_Setpoint = cooling_SetPoint - 11

        ECBC_standard_Idf.copyidfobject(new_VAV_System_obj)
        ECBC_standard_Idf.save()
    return ECBC_standard_Idf


# function to add HVACTemplate:Plant:ChilledWaterLoop from VAV Library
def add_System_C_Objects(ECBC_standard_Idf, ECBC_VAV_Library):
    print "copy HVACTemplate:Plant:ChilledWaterLoop in ECBC_standard_Idf"
    ECBC_standard_Idf = copy_objects(ECBC_VAV_Library, ECBC_standard_Idf, "HVACTemplate:Plant:ChilledWaterLoop".upper())
    print "copy HVACTemplate:Plant:Chiller in ECBC_standard_Idf"
    ECBC_standard_Idf = copy_objects(ECBC_VAV_Library, ECBC_standard_Idf, "HVACTemplate:Plant:Chiller".upper())
    print "copy HVACTemplate:Plant:Tower in ECBC_standard_Idf"
    ECBC_standard_Idf = copy_objects(ECBC_VAV_Library, ECBC_standard_Idf, "HVACTemplate:Plant:Tower".upper())
    print "copy HVACTemplate:Plant:HotWaterLoop in ECBC_standard_Idf"
    ECBC_standard_Idf = copy_objects(ECBC_VAV_Library, ECBC_standard_Idf, "HVACTemplate:Plant:HotWaterLoop".upper())
    print "copy HVACTemplate:Plant:Boiler in ECBC_standard_Idf"
    ECBC_standard_Idf = copy_objects(ECBC_VAV_Library, ECBC_standard_Idf, "HVACTemplate:Plant:Boiler".upper())
    return ECBC_standard_Idf


def creating_HVAC_SystemType_C(ECBC_standard_Idf, System_Zonelist, ECBC_VAV_Library, ECBC_Zonelist_Wise_Schedule,
                               zone_wise_buildingstory_details, cooling_SetPoint):
    print "get HVAC Zone List"
    # function call to fet zone wise details and unique schedule name
    HVAC_Zone_List, building_story_arr, buildingStory_Schedule_arr = get_HVAC_Zone_List(ECBC_standard_Idf,
                                                                                        System_Zonelist,
                                                                                        ECBC_Zonelist_Wise_Schedule,
                                                                                        zone_wise_buildingstory_details)
    # ECBC_standard_Idf = remove_Object_From_CSV(ECBC_standard_Idf)
    # ECBC_standard_Idf = add_Sizing_Parameter(ECBC_standard_Idf, ECBC_VAV_Library, heating_Sizing_Factor, cooling_Sizing_Factor)
    #
    VAV_name_list = []
    VAV_Count = 0

    for building_story in building_story_arr:
        schedulesPerFloor = [schedule_row[1] for schedule_row in buildingStory_Schedule_arr if
                             schedule_row[0] == building_story]

        for HVAC_schedule in schedulesPerFloor:
            zone_list = [row[0] for row in HVAC_Zone_List if row[3] == HVAC_schedule and row[6] == building_story]
            # print zone_list

            VAV_Count = VAV_Count + 1
            VAV_Name = "VAV_System_" + str(VAV_Count)
            VAV_name_list.append([VAV_Name, HVAC_schedule])
            for zone in zone_list:
                #                print zone

                zone_details = [zone_set_row for zone_set_row in HVAC_Zone_List if zone_set_row[0] == zone]
                #                #zone_name , heeting setpoint, cooling setpoint
                ECBC_standard_Idf = add_HVAC_Thermostat(ECBC_standard_Idf, ECBC_VAV_Library, zone_details[0][0],
                                                        zone_details[0][4], zone_details[0][5])
                print "Creating HVACTemplate:Zone:VAV object...."
                ECBC_standard_Idf = add_HVAC_Zone_VAV(ECBC_standard_Idf, ECBC_VAV_Library, zone_details[0][0], VAV_Name,
                                                      zone_details[0][1], zone_details[0][2], zone_details[0][3],
                                                      cooling_SetPoint)

    ECBC_standard_Idf = add_HVAC_System_VAV(ECBC_standard_Idf, ECBC_VAV_Library, VAV_name_list, cooling_SetPoint)
    ECBC_standard_Idf = add_System_C_Objects(ECBC_standard_Idf, ECBC_VAV_Library)
    return ECBC_standard_Idf
