def getIDDPath(version):
    IDD_Path = "C:/EnergyPlusV8-6-0/Energy+.idd"
    if(version == "9.0"):
        IDD_Path = "C:/EnergyPlusV9-0-0/Energy+.idd"
    if(version == "9.1"):
        IDD_Path = "C:/EnergyPlusV9-1-0/Energy+.idd"
      
    return IDD_Path

def getEplusPath(version):
    Eplus_path = "C:/EnergyPlusV8-6-0/RunEPlus"
    if(version == "9.0"):
        Eplus_path = "C:/EnergyPlusV9-0-0/RunEPlus"
    if(version == "9.1"):
        Eplus_path = "C:/EnergyPlusV9-1-0/RunEPlus"
        
    return  Eplus_path

