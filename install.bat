@echo off

title ECBC Baseline Generation Install Script!
echo ECBC Baseline Generation!
echo ************************* 

set root=%cd%
echo %root%
echo installing virtual env -------
pip install virtualenv


echo Creating Virtal env------
py -2 -m virtualenv ecbcenv

echo activating virtual env------
@Call ".\ecbcenv\Scripts\activate.bat"

echo creating source code folder--------
cd ecbcenv
mkdir source
cd source

set sourcefld=%cd%
echo %sourcefld%

echo copying source code--------

git clone https://Nikunjeds@bitbucket.org/Nikunjeds/ecbcbaselineautomation_v2.git

echo installing dependency software----
cd %sourcefld%\ecbcbaselineautomation_v2
@PIP install -r requirements.txt

echo dependency installation finish------

echo deactivating virtual env------
@Call "deactivate"

echo Creating run file---

echo changing working directory to root
cd %root%


@echo echo off > run.bat
@echo @Call ".\ecbcenv\Scripts\activate.bat" >>run.bat
@echo cd ecbcenv\source\ecbcbaselineautomation_v2\ECBCBaselineGen>> run.bat
@echo @python Main.py>> run.bat
@echo @Call "deactivate">> run.bat

echo run file created----
echo install script run successfully----


